package ist.challenge.andi_susanto.constant;

public class ResponseConstant {
    public static final String USERNAME_EXIST = "Username sudah terpakai";
    public static final String REGISTRATION_SUCCESS = "";
    public static final String USERNAME_PASSWORD_EMPTY = "Username dan / atau password kosong";
    public static final String USERNAME_PASSWORD_WRONG = "";
    public static final String LOGIN_SUCCESS = "Sukses Login";
    public static final String USER_NOT_FOUND = "User tidak ada";
    public static final String PASSWORD_SAME_AS_BEFORE = "Password tidak boleh sama dengan password sebelumnya";
    public static final String UPDATE_SUCCESS = "";
}
