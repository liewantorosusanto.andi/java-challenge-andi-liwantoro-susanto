package ist.challenge.andi_susanto.controller;

import ist.challenge.andi_susanto.dto.request.LoginRequest;
import ist.challenge.andi_susanto.dto.request.RegistrasiRequest;
import ist.challenge.andi_susanto.model.User;
import ist.challenge.andi_susanto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/registrasi")
    public ResponseEntity<String> registrasi(@RequestBody RegistrasiRequest request) {
        return userService.registrasi(request);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginRequest request) {
        return userService.login(request);
    }

    @GetMapping("/user")
    public ResponseEntity<List<User>> getUser() {
        return userService.listUser();
    }

    @PutMapping("/user")
    public ResponseEntity<String> putUser(@RequestBody User request){
        return userService.updateUser(request);
    }

    @GetMapping("/people")
    public ResponseEntity<String> getPeople(){
        return userService.integrate();
    }
}
