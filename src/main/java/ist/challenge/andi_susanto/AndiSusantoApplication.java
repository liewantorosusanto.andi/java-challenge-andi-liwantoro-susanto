package ist.challenge.andi_susanto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AndiSusantoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AndiSusantoApplication.class, args);
	}

}
