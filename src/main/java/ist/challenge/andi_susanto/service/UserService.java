package ist.challenge.andi_susanto.service;

import ist.challenge.andi_susanto.dto.request.LoginRequest;
import ist.challenge.andi_susanto.dto.request.RegistrasiRequest;
import ist.challenge.andi_susanto.model.User;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    ResponseEntity<String> registrasi(RegistrasiRequest registrasiRequest);
    ResponseEntity<String> login(LoginRequest loginRequest);
    ResponseEntity<List<User>> listUser();
    ResponseEntity<String> updateUser(User userRequest);
    ResponseEntity<String> integrate();
}
