package ist.challenge.andi_susanto.service.serviceimpl;

import ist.challenge.andi_susanto.constant.ResponseConstant;
import ist.challenge.andi_susanto.dto.request.LoginRequest;
import ist.challenge.andi_susanto.dto.request.RegistrasiRequest;
import ist.challenge.andi_susanto.model.User;
import ist.challenge.andi_susanto.repository.UserRepository;
import ist.challenge.andi_susanto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;
    RestTemplate restTemplate = new RestTemplate();

    @Override
    public ResponseEntity<String> registrasi(RegistrasiRequest registrasiRequest) {
        Optional<User> existingUser = userRepository.findByUsername(registrasiRequest.getUsername());
        if(existingUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ResponseConstant.USERNAME_EXIST);
        }

        User user = new User(registrasiRequest.getUsername(),registrasiRequest.getPassword());
        userRepository.save(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(ResponseConstant.REGISTRATION_SUCCESS);
    }

    @Override
    public ResponseEntity<String> login(LoginRequest loginRequest) {
        if(!loginRequest.isValid()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseConstant.USERNAME_PASSWORD_EMPTY);
        }

        Optional<User> user = userRepository.findByUsernameAndPassword(loginRequest.getUsername(),loginRequest.getPassword());
        if(user.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ResponseConstant.USERNAME_PASSWORD_WRONG);
        }

        return ResponseEntity.ok(ResponseConstant.LOGIN_SUCCESS);
    }

    @Override
    public ResponseEntity<List<User>> listUser() {
        List<User> users = userRepository.findAll();
        return ResponseEntity.ok(users);
    }

    @Override
    public ResponseEntity<String> updateUser(User userRequest) {
        Optional<User> existingUsername = userRepository.findByUsername(userRequest.getUsername());
        if(existingUsername.isPresent() &&
                !Objects.equals(existingUsername.get().getId(), userRequest.getId())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ResponseConstant.USERNAME_EXIST);
        }

        Optional<User> userOptional = userRepository.findById(userRequest.getId());
        if(userOptional.isEmpty()) {
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseConstant.USER_NOT_FOUND);
        }

        User user = userOptional.get();
        if(user.getPassword().equals(userRequest.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseConstant.PASSWORD_SAME_AS_BEFORE);
        }

        user.setPassword(userRequest.getPassword());
        user.setUsername(userRequest.getUsername());
        userRepository.save(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(ResponseConstant.UPDATE_SUCCESS);

    }

    @Override
    public ResponseEntity<String> integrate() {
        String thirdPartyUrl = "https://swapi.py4e.com/api/people/?search=";
        return restTemplate.exchange(thirdPartyUrl, HttpMethod.GET, null, String.class);
    }
}
