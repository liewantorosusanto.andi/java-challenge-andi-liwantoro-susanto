package ist.challenge.andi_susanto;

import com.fasterxml.jackson.databind.ObjectMapper;
import ist.challenge.andi_susanto.dto.request.RegistrasiRequest;
import ist.challenge.andi_susanto.repository.UserRepository;
import ist.challenge.andi_susanto.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@SpringBootTest
class AndiSusantoApplicationTests {
	@Autowired
	private WebApplicationContext webApplicationContext;
	private MockMvc mvc;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserService userService;

	@BeforeEach
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	ObjectMapper objectMapper = new ObjectMapper();

	@Test
	public void registrasi_success201() throws Exception {
		RegistrasiRequest registrasiRequest = new RegistrasiRequest("testing","password");

		mvc.perform(MockMvcRequestBuilders.post("/registrasi")
						.content(objectMapper.writeValueAsString(registrasiRequest))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated());
	}

	@Test
	public void registrasi_sameUsername_error409() throws Exception {
		RegistrasiRequest registrasiRequest = new RegistrasiRequest("testing","password");

		mvc.perform(MockMvcRequestBuilders.post("/registrasi")
						.content(objectMapper.writeValueAsString(registrasiRequest))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isConflict())
				.andExpect(MockMvcResultMatchers.content().string("Username sudah terpakai"));
	}
}
